from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.views import login, logout


urlpatterns = [

    url(r'^$', 'drugs.views.index', name='index'),
    #url(r'^drugsanddoses/', 'drugs.views.drugsanddoses', name='drugsanddoses'),
    url(r'^getdrugdoses/', 'drugs.views.getdrugdoses', name='getdrugdoses'),
    #url(r'^adddrugtolist/', 'drugs.views.adddrugtolist', name='adddrugtolist'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),
    url(r'^calculate-risk/', 'drugs.views.calculate_risk', name='calculate_risk'),
    #PublicationManager
    url(r'^publications/', 'publicationmanager.views.publications', name='publications'),
]
if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)