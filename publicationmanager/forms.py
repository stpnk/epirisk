from django import forms
from .models import Publications

class FindPublicationForm(forms.ModelForm):
	class Meta:
		model = Publications
		fields = ['pubTitle', 'pubAuthors', 'pubYear', 'publisherName', 'pubLink']