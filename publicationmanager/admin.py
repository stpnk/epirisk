from django.contrib import admin
from .models import Publications, Registers
from .forms import FindPublicationForm

class PublicationsManagerAdmin(admin.ModelAdmin):
	list_display = ["__unicode__", "pubTitle"]
	form = FindPublicationForm
	class Meta:
		model = Publications

admin.site.register(Registers)
admin.site.register(Publications)