# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publicationmanager', '0002_auto_20160308_1001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publications',
            name='pubLink',
            field=models.URLField(max_length=512, verbose_name=b'Website URL'),
        ),
        migrations.AlterField(
            model_name='registers',
            name='registerURL',
            field=models.URLField(max_length=512, verbose_name=b'Website URL'),
        ),
    ]
