# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('publicationmanager', '0003_auto_20160308_1004'),
    ]

    operations = [
        migrations.AddField(
            model_name='registers',
            name='registerLocation',
            field=models.CharField(default=datetime.datetime(2016, 3, 8, 10, 7, 13, 59570, tzinfo=utc), max_length=512, verbose_name=b'Region'),
            preserve_default=False,
        ),
    ]
