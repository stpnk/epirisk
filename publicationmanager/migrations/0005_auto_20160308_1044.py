# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publicationmanager', '0004_registers_registerlocation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publications',
            name='publisherName',
            field=models.CharField(max_length=128, verbose_name=b'Publisher'),
        ),
    ]
