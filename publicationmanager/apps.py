from django.apps import AppConfig

class PublicationManager(AppConfig):
    name = 'Publication Manager'
    verbose_name = 'Publication Manager'