from django.shortcuts import render
from .forms import FindPublicationForm
from Bio import Entrez

# Create your views here.

def references(request):
    data = Publications.objects.get.all().order_by('pubTitle')
    # data_json = serializers.serialize('json', data, 
    #                                     fields=('drug_name', 'l_dose', 'h_dose', 'dose_disp', 'extra_info', 'pubmed_id', 'risk'))
    
    # return HttpResponse(data_json, content_type="application/json")
    return HttpResponse(data, content_type="application/json")

def publications(request):
	title = 'Add Publications'

	#add a form
	form = FindPublicationForm(request.POST or None)
	context = {
		"title": title,
		"form": form
	}

	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		context = {
			"title": "Successfully added",
			"form": form
		}

	return render(request, "publications.html", context)

def search(query):
    Entrez.email = 'your.email@example.com'
    handle = Entrez.esearch(db='pubmed', 
                            sort='relevance', 
                            retmax='20',
                            retmode='xml', 
                            term=query)
    results = Entrez.read(handle)
    return results

def fetch_details(id_list):
    ids = ','.join(id_list)
    Entrez.email = 'your.email@example.com'
    handle = Entrez.efetch(db='pubmed',
                           retmode='xml',
                           id=ids)
    results = Entrez.read(handle)
    return results

if __name__ == '__main__':
    results = search('fever')
    id_list = results['IdList']
    papers = fetch_details(id_list)
    for i, paper in enumerate(papers):
        print("%d) %s" % (i+1, paper['MedlineCitation']['Article']['ArticleTitle']))
    # Pretty print the first paper in full
    #import json
    #print(json.dumps(papers[0], indent=2, separators=(',', ':')))