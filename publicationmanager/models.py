# encoding: utf-8

from django import forms
from django.template.loader import render_to_string
from django.db import models
import uuid
import datetime

YEAR_CHOICES = []
for r in range(1900, (datetime.datetime.now().year+1)):
    YEAR_CHOICES.append((r,r))

class Registers(models.Model):
	registerName = models.CharField("Register Name", max_length=512)
	registerLocation = models.CharField("Region", max_length=512)
	registerID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	registerBio = models.TextField("Bio", blank=True, null=True)
	registerContactInfo = models.TextField("Contact Information", blank=True, null=True)
	registerURL = models.URLField("Website URL", max_length=512)
	registerExtraInfo = models.TextField("Additional Information", blank=True, null=True)

	def __unicode__(self): #Python 3.3 is __str__
		return "%s" % ( self.registerName )

	class Meta:
		verbose_name = "Register"
		verbose_name_plural = "Registers"


class Publications(models.Model):
	pubTitle = models.CharField("Publication Title", max_length=512)
	pubAuthors = models.CharField("Authors", max_length=256)
	pubYear = models.IntegerField("Publication Year", choices=YEAR_CHOICES, default=datetime.datetime.now().year)
	publisherName = models.CharField("Publisher", max_length = 128)
	pubLink = models.URLField("Website URL", max_length=512)


	def __unicode__(self): #Python 3.3 is __str__
		return "%s (%s)" % ( self.pubTitle, self.pubYear )

	class Meta:
		verbose_name = "Publication"
		verbose_name_plural = "Publications"