from django.shortcuts import render
from .forms import SignUpForm

# Create your views here.
def home(request):
	title = "Welcome"
	form = SignUpForm(request.POST or None)
	context = {
		"title": title,
		"form": form
	}

	#Check if user is logged in and if so, display different title
	if request.user.is_authenticated():
		title = "Welcome Back,  %s" %(request.user)
	
	#Add a form
	if form.is_valid():
		instance = form.save(commit = False)

		#Validations
		full_name = form.cleaned_data.get("full_name")
		if not full_name:
			full_name = "New Full Name"
		instance.full_name = full_name

		instance.save()
		context = {
			"title": "Thank You"
		}
	





	return render(request, "base.html", context)

def riskcalculator(request):
	return render(request, "base.html/#riskcalculator", context)

def drugsandrisks(request):
	return render(request, "base.html/#drugsandrisks", context)

def about(request):
	return render(request, "base.html/#about", context)

def references(request):
	return render(request, "base.html/#references", context)

def contact(request):
	return render(request, "base.html/#contact", context)