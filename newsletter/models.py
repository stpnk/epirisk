from django.db import models

# Create your models here.
class SignUp(models.Model):
	email = models.EmailField(blank=False, null=True, max_length=120)
	full_name = models.CharField(blank=False, null=True, max_length=120)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __unicode__(self): #Python 3.3 is __str__
		return self.full_name + " (" + self.email +")"

	class Meta:
		verbose_name = "Sign-Up"
		verbose_name_plural = "Sign-Ups"
