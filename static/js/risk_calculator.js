$(function(){
    $('.drugName').on('click', getDrugDoses);
    $('#drugDoseList').on('click', 'a.drugDoseRange', addToList);
    $('#selectedDrugs').on('click', 'a.selectedDrugsItemLink', removeFromList);
});


function resetForm(){
    $('#selectDrugTitle2').attr('class', 'col-lg-12 text-center');
    $('#drugNameList').attr('class', 'col-lg-6 text-left col-centered col-md-offset-3');
    $('#drugDoseList').attr('class', 'hidden');
    $('#activeSelectedDrugList').attr('class', 'hidden');
    $('#cumulativeRiskResultDiv').attr('class', 'hidden');
    $('#progressBarCalculator').attr('style', 'width:0%');

    $('.drugDoseRange').remove();
    $('.selectedDrugsItem').remove();
    $('#cumulativeRiskResult').empty();
    $('#referenceTableDiv').empty();
}

function removeFromList(){
    $(this).closest('span').remove();
    $('#cumulativeRiskResult').empty();
    $('#referenceTableDiv').empty();
}


function addToList() {
    $('#activeSelectedDrugList').css('visibility','visible').hide().fadeIn("600");
    // Check if item already in list
    var inList = false,
        id = this.id; 
    $('#riskList span.selectedDrugsItem').each (function () {
        if(this.id == id){
            inList = true;
            return
        }
    });

    if (this.id !== '' && inList==false){
        $('#cumulativeRiskResult').empty();
        var drugDoseItem = document.createElement('span'),
            icon = '<a class="selectedDrugsItemLink pull-right" href="javascript:void(0)"><span class="glyphicon glyphicon-remove text-right" aria-hidden="true"></span></a>',
        drug_name = $(this).attr('data-drug-name'),
        risk = $(this).attr('data-drug-risk');
        drugDoseItem.innerHTML = drug_name + ' (' + this.innerHTML + ')' + icon;
        drugDoseItem.setAttribute('class', 'list-group-item selectedDrugsItem');
        drugDoseItem.setAttribute('id', this.id);
        drugDoseItem.setAttribute('data-drug-name', $(this).attr('data-drug-name'));
        drugDoseItem.setAttribute('data-drug-risk', risk);
        drugDoseItem.setAttribute('data-pubmed-id', $(this).attr('data-pubmed-id'));
        drugDoseItem.setAttribute('data-dose-disp', $(this).attr('data-dose-disp'));
        $('#riskList').append(drugDoseItem);

        $('#drugNameList').attr('class', 'col-lg-4 text-left');
        $('#drugDoseList').attr('class', 'col-lg-4 text-left');
        $('#activeSelectedDrugList').attr('class', 'col-lg-4 text-left');
        $('#progressBarCalculator').attr('style', 'width:66%');
        //$('#selectDrugTitle2').attr('class', 'hidden');

    }
    window.location.href = '#calculate';
};

function getDrugDoses() {
    var drugID = this.id,
        drugNameValue = this.innerHTML;

    $('.drugDoseRange').remove();
    $('.drugDoseRangeItem').remove();
    $('#drugDoseList').css('visibility','visible').hide().fadeIn("600");
    
    	ga('send', {
	  hitType: 'event',
	  eventCategory: 'Calculator',
	  eventAction: 'calculated',
	  eventLabel: 'epiRisk Calculations'
	});

    if (data_json.length !== 0){
        $.each(data_json, function(index, object){
            if(object['id'] == drugID){
                var drugName = object['name'];
                if (object['doses'].length !==0){
                    $.each(object['doses'], function(index, dose){

                        var drugDoseItem = document.createElement('a');
                        drugDoseItem.setAttribute('class', 'list-group-item drugDoseRange');
                        drugDoseItem.setAttribute('id', dose['pk']);

                        drugDoseItem.innerHTML = dose['dose_disp'];

                        drugDoseItem.setAttribute('data-drug-name', drugName);
                        drugDoseItem.setAttribute('data-drug-risk', dose['risk']);
                        drugDoseItem.setAttribute('data-pubmed-id', dose['pubmed_id']);
                        drugDoseItem.setAttribute('data-dose-disp', dose['dose_disp']);

                        $('#doseList').append(drugDoseItem);

                        $('#drugNameList').attr('class', 'col-lg-4 text-left');
                        $('#drugDoseList').attr('class', 'col-lg-4 text-left');
                        //$('#activeSelectedDrugList').attr('class', 'hidden');
                        $('#progressBarCalculator').attr('style', 'width:33%');
                        //$('#selectDrugTitle2').attr('class', 'hidden');
                    });
                } else {
                    var drugDoseItem = document.createElement('a');
                    drugDoseItem.setAttribute('class', 'list-group-item drugDoseRange');
                    drugDoseItem.innerHTML = 'No doses currently in database.'
                    $('#doseList').append(drugDoseItem);
                }
            }
        });
    }
    window.location.href = '#calculate';    
};

        
function CalculateRisk(){
    $('#selectDrugTitle2').attr('class', 'hidden');
    $('#drugNameList').attr('class', 'col-lg-3 text-left hidden');
    $('#drugDoseList').attr('class', 'col-lg-3 text-left hidden');
    $('#activeSelectedDrugList').attr('class', 'col-lg-3 text-left hidden');
    $('#cumulativeRiskResultDiv').attr('class', 'col-lg-6 text-left col-md-offset-3'); 
    $('#progressBarCalculator').attr('style', 'width:100%');
    $('#cumulativeRiskResultDiv').css('visibility','visible').hide().fadeIn("600"); 

    $('#cumulativeRiskResult').empty();
    $('#referenceTableDiv').empty();

    // Calculate cumulative risk
    //var cumulativeRisk = 0,
    var    doses_ids = [];
    $('#riskList span.selectedDrugsItem').each(function(){
        doses_ids.push($(this).attr('id'))
    //    cumulativeRisk = cumulativeRisk + parseFloat($(this).attr('data-drug-risk'));
    });
    
    $.ajax({
        url: "calculate-risk/",
        data: {'doses_ids': doses_ids.join(",")},
    }).done(function(data){
        $('#cumulativeRiskResult').text(data.cumulative_risk + '%');
    })
    
    if ($('#riskList span.selectedDrugsItem').length !==0){
        //Create reference table
        referenceTable = document.createElement('table');
        referenceTable.setAttribute('id', 'referenceTable');
        referenceTable.setAttribute('class', 'table table-bordered table-hover');
        $('#riskList span.selectedDrugsItem').each(function(){
            var row = document.createElement('tr'),
                drug_name = '<td>' + $(this).attr('data-drug-name') + '</td>',
                dose_range = '<td>' + $(this).attr('data-dose-disp') + '</td>'
                //dose_range = '<td>' + $(this).attr('data-drug-l-dose') + '—' + $(this).attr('data-drug-h-dose') + 'mg </td>',
                reference = '<td><a href="http://www.ncbi.nlm.nih.gov/pubmed/' + $(this).attr('data-pubmed-id') + '"target="_blank">Reference</a></td>';
            $(row).append(drug_name + dose_range + reference);
            $(referenceTable).append(row);
        });
        $(referenceTableDiv).append('<h5>References</5h>');
        $(referenceTableDiv).append(referenceTable);
    } else {
        $('#cumulativeRiskResult').empty();
        $('#referenceTableDiv').empty();
        $('#cumulativeRiskResult').append('No drugs in list');
    }
    window.location.href = '#calculate';
};