function NeedDrugInformation() {
    $('#infoTable').find('tr.infoTableRow').remove();
    $('#noDoses').remove()
    var drugID = $('#drugSet option:selected').attr('id');


    if (data_json.length !== 0){
        $.each(data_json, function(index, object){
            if(object['id'] == drugID){
                if (object['doses'].length !==0){
                    $.each(object['doses'], function(index, dose){
                        dose_range = '<td>' + dose['dose_disp'] + '</td>',
                        
                        pubmed_id = dose['pubmed_id'],
                        pubmed_link = '<td class="text-left"><a href="http://www.ncbi.nlm.nih.gov/pubmed/' + pubmed_id + '" target="_blank">Link</a></td>';
                        
                        risk = '<td class="text-left">' + dose['risk'] + '</td>',

                        row = document.createElement('tr');
                        $(row).attr('class', 'infoTableRow')

                        $(row).append(dose_range + risk + pubmed_link);
                        $('#infoTable').append(row);
                        $('#infoTable').attr('style', 'display:table;')
                    });
                } else {
                    $('#infoTable').attr('style', 'display:none;')
                    $('#drugsAndRisksDiv').append('<h4 id="noDoses">No doses currently in database.</h4>')
                }
            }
        });
    }
}