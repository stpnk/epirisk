# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Antiepileptics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=120, null=True, verbose_name=b'Drug Name', blank=True)),
                ('timestamp', models.DateField(auto_now_add=True, verbose_name=b'Date Added')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name=b'Last Updated')),
            ],
            options={
                'verbose_name': 'Drug',
                'verbose_name_plural': 'Drugs',
            },
        ),
        migrations.CreateModel(
            name='RiskCalculator',
            fields=[
                ('drugID', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('l_dose', models.IntegerField(verbose_name=b'Lower Dose Limit (mg)')),
                ('h_dose', models.IntegerField(verbose_name=b'Upper Dose Limit (mg)')),
                ('dose_disp', models.CharField(max_length=34, verbose_name=b'Label')),
                ('pubmed_id', models.IntegerField(verbose_name=b'PubMed ID')),
                ('extra_info', models.TextField(null=True, verbose_name=b'Extra Information', blank=True)),
                ('risk', models.DecimalField(verbose_name=b'Risk (%)', max_digits=5, decimal_places=2)),
                ('timestamp', models.DateField(auto_now_add=True, verbose_name=b'Date Added')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name=b'Last Updated')),
                ('drug_name', models.ForeignKey(to='drugs.Antiepileptics')),
            ],
            options={
                'verbose_name': 'Dose',
                'verbose_name_plural': 'Doses',
            },
        ),
    ]
