# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('drugs', '0002_drugcombination'),
    ]

    operations = [
        migrations.CreateModel(
            name='Combination',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.SlugField()),
                ('dose_disp', models.CharField(max_length=34, verbose_name=b'Label')),
                ('risk', models.DecimalField(verbose_name=b'Risk (%)', max_digits=5, decimal_places=2)),
            ],
        ),
        migrations.RemoveField(
            model_name='drugcombination',
            name='dose_disp',
        ),
        migrations.RemoveField(
            model_name='drugcombination',
            name='id',
        ),
        migrations.RemoveField(
            model_name='drugcombination',
            name='name',
        ),
        migrations.RemoveField(
            model_name='drugcombination',
            name='risk',
        ),
        migrations.CreateModel(
            name='DoseCombination',
            fields=[
                ('combination_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='drugs.Combination')),
                ('dose', models.ManyToManyField(to='drugs.RiskCalculator')),
            ],
            bases=('drugs.combination',),
        ),
        migrations.AddField(
            model_name='drugcombination',
            name='combination_ptr',
            field=models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, default=1, serialize=False, to='drugs.Combination'),
            preserve_default=False,
        ),
    ]
