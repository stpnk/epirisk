# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('drugs', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DrugCombination',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.SlugField()),
                ('dose_disp', models.CharField(max_length=34, verbose_name=b'Label')),
                ('risk', models.DecimalField(verbose_name=b'Risk (%)', max_digits=5, decimal_places=2)),
                ('drug', models.ManyToManyField(to='drugs.Antiepileptics')),
            ],
        ),
    ]
