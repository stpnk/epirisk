from django.contrib import admin
from .models import Antiepileptics, RiskCalculator, DrugCombination, DoseCombination, Combination

class AntiepilepticsAdmin(admin.ModelAdmin):
	list_display = ["__unicode__", "timestamp","updated"]
	class Meta:
		model = Antiepileptics

class CombinationAdmin(admin.ModelAdmin):
    """docstring for DrugCombination"""

    list_display = ('name', 'dose_disp', 'risk',)

class RiskCalculatorAdmin(admin.ModelAdmin):

    list_display = ('__unicode__', 'risk')

admin.site.register(Antiepileptics, AntiepilepticsAdmin)
admin.site.register(RiskCalculator,RiskCalculatorAdmin)
admin.site.register(DrugCombination, CombinationAdmin)
admin.site.register(DoseCombination, CombinationAdmin)
