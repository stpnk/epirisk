from django.apps import AppConfig

class YourAppConfig(AppConfig):
    name = 'drugs'
    verbose_name = 'Drugs'