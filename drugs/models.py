# encoding: utf-8

from django import forms
from django.template.loader import render_to_string
from django.db import models
import uuid
import datetime


YEAR_CHOICES = []
for r in range(1900, (datetime.datetime.now().year+1)):
    YEAR_CHOICES.append((r,r))

# Create your models here.
class Antiepileptics(models.Model):
	name = models.CharField("Drug Name", blank=True, null=True, max_length=120)
	timestamp = models.DateField("Date Added", auto_now_add=True, auto_now=False)
	updated = models.DateTimeField("Last Updated", auto_now_add=False, auto_now=True)


	def get_doses(self):
		"""Get all doses for drug"""
		return {
			'id':self.id,
			'name': self.name,
			'doses': [{
				'pk': unicode(dose.pk),
				'dose_disp': dose.dose_disp, 
				'pubmed_id': dose.pubmed_id,
				'risk': unicode(dose.risk)
			} for dose in self.riskcalculator_set.all()]
		}


	def __unicode__(self): #Python 3.3 is __str__
		return self.name

	class Meta:
		verbose_name = "Drug"
		verbose_name_plural = "Drugs"

class RiskCalculator(models.Model):

	TYPE1 = u'<'
	TYPE2 = u'>'
	TYPE3 = u'\u2265'
	TYPE4 = u'\u2264'

	COMPARITOR_CHOICES = ((TYPE1, u'<'),
	                    (TYPE2, u'>'),
	                    (TYPE3, u'\u2265'),
	                    (TYPE4, u'\u2264'))

	drug_name = models.ForeignKey(Antiepileptics, on_delete=models.CASCADE)
	drugID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	l_dose = models.IntegerField("Lower Dose Limit (mg)")
	h_dose = models.IntegerField("Upper Dose Limit (mg)")
	dose_disp = models.CharField("Label", max_length=34)
	pubmed_id = models.IntegerField("PubMed ID")
	extra_info = models.TextField("Extra Information", blank=True, null=True)
	risk = models.DecimalField("Risk (%)", decimal_places=2, max_digits=5)
	timestamp = models.DateField("Date Added", auto_now_add=True, auto_now=False)
	updated = models.DateTimeField("Last Updated", auto_now_add=False, auto_now=True)

	def __unicode__(self): #Python 3.3 is __str__
		return "%s - %s" % ( self.drug_name, self.dose_disp )

	class Meta:
		verbose_name = "Dose"
		verbose_name_plural = "Doses"



class Combination(models.Model):
	"""Base combination model"""
	name = models.SlugField()
	dose_disp = models.CharField("Label", max_length=34)
	risk = models.DecimalField("Risk (%)", decimal_places=2, max_digits=5)	

	def __unicode__(self):
		return self.name


class DrugCombination(Combination):
	drug = models.ManyToManyField(Antiepileptics)
	

class DoseCombination(Combination):
	dose = models.ManyToManyField(RiskCalculator)


# class Publications(models.Model):
# 	drugName = models.ForeignKey(Antiepileptics, on_delete=models.CASCADE)
# 	doseRange = models.ForeignKey(RiskCalculator, on_delete=models.CASCADE)
# 	pubTitle = models.CharField("Publication Title", max_length=512)
# 	pubAuthors = models.CharField("Authors", max_length=256)
# 	pubYear = models.IntegerField("Publication Year", choices=YEAR_CHOICES, default=datetime.datetime.now().year)
# 	publisherName = models.CharField("Publication", max_length = 128)
# 	pubLink = models.CharField("PubMed Link", max_length=512)


# 	def __unicode__(self): #Python 3.3 is __str__
# 		return "%s - %s (%s)" % ( self.drug_name, self.pubTitle, self.pubYear )

# 	class Meta:
# 		verbose_name = "Publication"
# 		verbose_name_plural = "Publications"




# class Registries(models.Model):
# 	registryID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
# 	registryLabel = models.CharField("Registry Name", max_length=128)
# 	reminderText = models.TextField("Reminder Text")
# 	contactInfo = models.Text("Contact Information")

# 	class Meta:
# 		verbose_name = "Registry"
# 		verbose_name_plural = "Registries"

# 	def __unicode__(self): #Python 3.3 is __str__
# 		return "%s - %s" % ( self.registryLabel)


