from django.core import serializers
from django.shortcuts import render
from .models import RiskCalculator, Antiepileptics, DrugCombination, DoseCombination
from django.http import HttpResponse, HttpRequest, Http404, JsonResponse
import simplejson
import json
from django.contrib.auth.decorators import login_required
import time
from django.db.models import Sum


@login_required(login_url='login')
def index(request):

	
	data = [drug.get_doses() for drug in Antiepileptics.objects.all()]
	data_json = json.dumps(data)
	context = {
		'data': data,
		'data_json' : data_json,
	}	
	return render(request, "base.html", context)
	

	'''
	start = time.time()
	data = []

	drugs = Antiepileptics.objects.all().order_by('name')
	doses = RiskCalculator.objects.all()

	for drug in drugs:
		drug_dict = {}
		drug_dict['id'] = drug.id
		drug_dict['name'] = drug.name
		drug_doses = doses.filter(drug_name=drug)
		if drug_doses:
			drug_dict['doses'] = [
				{
					'dose_disp': dose.dose_disp, 
					'pubmed_id': dose.pubmed_id,
					'risk': unicode(dose.risk)
				} for dose in drug_doses
			]
		else:
			drug_dict['doses'] = []
		data.append(drug_dict)
	
	data = json.dumps(data)

	result_time = time.time()-start
	print data
	print result_time
	'''


def getdrugdoses(request):
	"""Get drug doses"""
	drug_name = request.GET.get('drugID')
	data = Antiepileptics.objects.get(name=drug_name).riskcalculator_set.all().order_by('l_dose')
	data_json = serializers.serialize('json', data, 
										fields=(
											'drug_name', 'l_dose', 'h_dose', 'dose_disp', 
											'extra_info', 'pubmed_id', 'risk'
										))
	
	return HttpResponse(data_json, content_type="application/json")


def calculate_risk(request):
	"""Cumulative risk calculation"""
	doses_ids = request.GET.get('doses_ids')
	doses_ids = doses_ids.split(',')
	doses  = RiskCalculator.objects.filter(pk__in=doses_ids).select_related()
	

	doses_combinations = DoseCombination.objects.all().select_related()
	drugs_combinations = DrugCombination.objects.all().select_related()

	cumulative_risk = 0

	if not doses_combinations and not drugs_combinations:
		risk_sum = doses.aggregate(Sum('risk'))
		cumulative_risk = risk_sum['risk__sum']

	else:
		# Calculate risk including doses combinations
		doses_list = list(doses)
		
		counted_doses = set()
		for c in doses_combinations:
			dose_comb = list(c.dose.all())
			if set(dose_comb).issubset(set(doses_list)):
				counted_doses.update(dose_comb)
				cumulative_risk += c.risk
		single_doses = set(doses_list) - counted_doses
		for d in single_doses:
			cumulative_risk += d.risk 

		# Calculate risk of drug combinations
		drugs = [dose.drug_name for dose in doses]
		drugs_list = list(set(drugs))
		for c in drugs_combinations:
			drug_combination = list(c.drug.all())
			if set(drug_combination).issubset(set(drugs)):
				cumulative_risk += c.risk

	return JsonResponse({'cumulative_risk': cumulative_risk})


