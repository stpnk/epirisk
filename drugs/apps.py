from django.apps import AppConfig

class YourAppConfig(AppConfig):
    name = 'antiepileptics'
    verbose_name = 'Antiepileptics'
